# SIGL 2019 - NLPF v1 - Bob Super Awesome Website

## About the project

### Goal

## Authors

This application is designed and developped by group from SIGL
2019 class, composed of
the following members:
* D'ESTE Vivien
* HUYNH Quang Nghi
* LY Christopher
* SOLIGNAC Louis-Francois

### Features

This application provides the following features:
* Interface 

### Teachnology used:

* Back-end: NodeJS (8.11.3)
* Front-End: Bootstrap (3.3.0)
* Database: PostgreSQL (9.5.10)


## Configuration

### Project's Configuration

All configurations are set in the configuration file located in `config/config.json`<br>

If you want to change the deployment environment, specify the desire environment
in the command line as described below in the section [To launch the
server](https://gitlab.com/shiinsekai/nlpf-v1#to-launch-the-server)

### PostGreSQL

Before starting the app, please make sure that a database is available with the
following configurations:
* User name: bob
* User password: 123456
* Database name: nlpf_v1

If there's no database available, here's the command lines to create one:<br>
`$ psql postgres`<br>
`postgres=# CREATE USER bob WITH PASSWORD '123456' SUPERUSER;`<br>
`postgres=# CREATE DATABASE nlpf_v1 OWNER bob;`<br>
`postgres=# \q`<br>

Here's the command lines to initiate the database:<br>
`$ psql nlpf_v1`<br>
`nlpf_v1=# \i sql/init.sql`

### NodeJS

#### To download the needed packages

Go to the root directory of the project<br>
`$ npm install`

#### To launch the server

`$ npm start server.js <environment>`

Here's the list of available environments:
* Local
* Development
* Production

### Outlook Office365 identification

The `passport-outlook` package was used to handle Outlook identification.<br>
Don't forget to register the application on the following link [https://apps.dev.microsoft.com/](https://apps.dev.microsoft.com/)

### Generating a self-signed openSSL certificate
Several steps to generate a self-signed certificate related to https:
* `$ cd certificates/`
* `$ ./certificats-generation.sh`

_Remark: Don't push the generated certificates on the Git as it is proper to each machine._
