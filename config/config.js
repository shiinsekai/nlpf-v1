//On deployment, please modify the environmentv
const env = require('./../server.js').env;

const lodash = require('lodash');

const config = require('./config.json');
const defaultConfig = config.development;

const environment = process.env.NODE_ENV || env;
const environmentConfig = config[environment];
const finalConfig = lodash.merge(defaultConfig, environmentConfig);

global.gConfig = finalConfig;

console.log('Configuration loaded for environment <' + environment + '>:');
console.log(JSON.stringify(global.gConfig, null, 4));
