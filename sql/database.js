let { Pool } = require('pg');

const pool = new Pool({
  host: global.gConfig.database_address,
  port: global.gConfig.database_port,
  database: global.gConfig.database,
  user: global.gConfig.database_user,
  password: global.gConfig.database_user_password
});

module.exports = pool;
