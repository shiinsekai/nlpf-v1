DROP TABLE IF EXISTS utilisateurs CASCADE;
DROP TABLE IF EXISTS batiment CASCADE;
DROP TABLE IF EXISTS tickets CASCADE;

CREATE TABLE utilisateurs (
  "id" SERIAL,
  "nom" varchar(128) NOT NULL,
  "prenom" varchar(128) NOT NULL,
  "telephone" VARCHAR(10),
  "adresse_email" varchar(128) NOT NULL unique,
  "outlook_id" varchar(128) unique,
  "admin" boolean,
  "blacklist" boolean,
  "access_token" varchar(2560),

  PRIMARY KEY(adresse_email)
);

CREATE TABLE batiment(
  "id" SERIAL,
  "adresse" varchar(128) NOT NULL,
  "code_postal" int,
  "ville" VARCHAR(128),
  "adresse_email_client" varchar(128) REFERENCES utilisateurs(adresse_email) ON DELETE CASCADE,
  "nom" varchar(128),

  PRIMARY KEY(id),
  UNIQUE(adresse, code_postal)
);

CREATE TABLE tickets(
  "id" SERIAL,
  "photo" varchar(128),
  "id_batiment" int REFERENCES batiment(id) ON DELETE CASCADE,
  "orientation" VARCHAR(32) NOT NULL,
  "adresse_email_client" varchar(128) REFERENCES utilisateurs(adresse_email) ON DELETE CASCADE,
  "etage" int NOT NULL,
  "date_intervention" DATE,
  "time_intervention" TIME,
  "status" VARCHAR(16),
  "description" varchar(128),
  "reason_rejection" varchar(128),

  PRIMARY KEY(id),
  UNIQUE(date_intervention, time_intervention)
);


INSERT INTO utilisateurs VALUES
    (DEFAULT, 'BOB', 'Bob', '0612234556', 'quangnghi.huynh@epita.fr', NULL, TRUE, FALSE), -- Admin
    (DEFAULT, 'BOB', 'Bob', '0612234556', 'louis-francois.solignac@epita.fr', NULL, TRUE, FALSE), -- Admin
    (DEFAULT, 'Christopher', 'LY', '0607018313', 'christopher.ly@epita.fr', NULL, TRUE, FALSE), -- Admin
    (DEFAULT, 'Client', '1', '0123456789', 'client1@nlpf.com', NULL, FALSE, FALSE),
    (DEFAULT, 'Client', '2', '0223456789', 'client2@nlpf.com', NULL, FALSE, FALSE),
    (DEFAULT, 'Client', '3', '0323456789', 'client3@nlpf.com', NULL, FALSE, TRUE);   -- Blacklisted

INSERT INTO batiment VALUES
    (DEFAULT, '66 rue guy moquet', 94800, 'Villejuif', 'client2@nlpf.com', 'EPITA VJ'),
    (DEFAULT, '14 rue voltaire', 94270, 'Le Kremlin-Bicetre', 'client1@nlpf.com', 'EPITA KB'),
    (DEFAULT, '11 rue voltaire', 94270, 'Le Kremlin-Bicetre', 'christopher.ly@epita.fr', 'EPITA KB'),
    (DEFAULT, '12 rue voltire', 9470, 'Le Kremli-Bicetre', 'christopher.ly@epita.fr', 'EPITA K');


INSERT INTO tickets VALUES
    (DEFAULT, NULL, 1, 'nord-ouest', 'client2@nlpf.com', 1, TO_DATE('20/12/2018', 'DD/MM/YYYY'), '12:00', 'accepted'), -- Attention syntaxe : mm/dd/yyyy
    (DEFAULT, NULL, 2, 'sud', 'client1@nlpf.com', 6, TO_DATE('21/12/2018', 'DD/MM/YYYY'), '15:00', 'accepted'),
    (DEFAULT, NULL, 1, 'sud', 'client2@nlpf.com', 2, TO_DATE('23/12/2018', 'DD/MM/YYYY'), '15:00', 'accepted'),
    (DEFAULT, NULL, 2, 'est', 'client1@nlpf.com', 6, TO_DATE('25/12/2018', 'DD/MM/YYYY'), '15:00', 'refused'),
    (DEFAULT, NULL, 2, 'ouest', 'client1@nlpf.com', 7, TO_DATE('01/01/2019', 'DD/MM/YYYY'), '15:00', 'reviewing');


-- Update the outlookId
CREATE OR REPLACE FUNCTION update_user_id(userEmail VARCHAR(128), outlookId VARCHAR(128), accessToken VARCHAR(2560))
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * FROM utilisateurs WHERE adresse_email = userEmail;
    IF NOT FOUND THEN
        INSERT INTO utilisateurs VALUES
            (DEFAULT, '', '', '', userEmail, outlookId, FALSE, FALSE, accessToken);
        RETURN TRUE;
    END IF;

    UPDATE utilisateurs SET outlook_id = outlookId, access_token = accessToken WHERE adresse_email = userEmail;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<update_user_id> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

-- Checking accounts
CREATE OR REPLACE FUNCTION check_account(userEmail VARCHAR(128))
    RETURNS VARCHAR(16) AS
$$
DECLARE
    isAdmin         BOOLEAN;
    isBlacklist     BOOLEAN;
    name            VARCHAR(128);
BEGIN
    name := NULL;
    SELECT admin, blacklist, nom INTO isAdmin, isBlacklist, name FROM utilisateurs WHERE adresse_email = userEmail;
    IF name = '' THEN
        RETURN 'new';
    ELSIF isAdmin = TRUE THEN
        RETURN 'admin';
    ELSIF isBlacklist = TRUE THEN
        RETURN 'blacklist';
    ELSE
        RETURN 'client';
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<check_account> failed: %', SQLERRM;
        RETURN '';
END
$$ LANGUAGE plpgsql;

--créer user
CREATE OR REPLACE FUNCTION create_user(nom varchar(128), prenom varchar(128), telephone VARCHAR(10), adresse_email varchar(128), 
                                        outlook_id varchar(128), admin boolean, blacklist boolean, accessToken varchar(2560))
    RETURNS BOOLEAN AS
$$
BEGIN
    INSERT INTO utilisateurs VALUES
        (DEFAULT, nom, prenom, telephone, adresse_email, outlook_id, admin, blacklist, accessToken);
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<create_user> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

--modifier user
CREATE OR REPLACE FUNCTION update_user(name varchar(128), firstname varchar(128), phone VARCHAR(10), email varchar(128), 
                                        adminrole boolean, blacklistrole boolean, accessToken varchar(2560))
    RETURNS BOOLEAN AS
$$
BEGIN
    UPDATE utilisateurs SET nom = name, prenom = firstname, telephone = phone, admin = adminrole,  blacklist = blacklistrole, access_token = accessToken
    WHERE adresse_email = email;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<update_user> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

--créer un bâtiment
CREATE OR REPLACE FUNCTION create_building(address varchar(128), zipcode int, city VARCHAR(128), email_client varchar(128), name varchar(128))
    RETURNS BOOLEAN AS
$$
BEGIN
    INSERT INTO batiment VALUES
        (DEFAULT, address, zipcode, city, email_client, name);
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<create_building> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

--update un bâtiment
CREATE OR REPLACE FUNCTION update_building(id_batiment int, address varchar(128), zipcode int, city VARCHAR(128), name varchar(128))
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * from batiment where id= id_batiment; 
    IF NOT FOUND THEN 
        RETURN FALSE;
    END IF;
    UPDATE batiment SET adresse = address, code_postal = zipcode, ville = city, nom = name
    WHERE id = id_batiment;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<update_building> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

--supprime un bâtiment
CREATE OR REPLACE FUNCTION delete_building(id_batiment int)
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * from batiment where id= id_batiment; 
    IF NOT FOUND THEN 
        RETURN FALSE;
    END IF;
    DELETE from batiment
    WHERE id = id_batiment;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<delete_building> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

--créer un ticket
CREATE OR REPLACE FUNCTION create_ticket(pic bytea, id_building int, b_orientation VARCHAR(32), email_client varchar(128), floor int, date_intervention DATE, time_intervention TIME, status VARCHAR(16), description_ticket VARCHAR(128))
    RETURNS BOOLEAN AS
$$
BEGIN
    INSERT INTO tickets VALUES
        (DEFAULT, pic, id_building, b_orientation, email_client, floor, date_intervention, time_intervention, status, description_ticket);
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<create_ticket> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

-- update un ticket
CREATE OR REPLACE FUNCTION update_ticket(id_ticket int, pic bytea, b_orientation VARCHAR(32), floor int, dateIntervention DATE, timeIntervention TIME, newStatus VARCHAR(16))
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * from tickets where id= id_ticket; 
    IF NOT FOUND THEN 
        RETURN FALSE;
    END IF;
    UPDATE tickets SET photo = pic, orientation = b_orientation, etage = floor, date_intervention = dateIntervention, time_intervention = timeIntervention, status = newStatus, description = ticket_description
    WHERE id = id_ticket;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<update_ticket> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

-- supprimer un ticket --
CREATE OR REPLACE FUNCTION delete_ticket(id_ticket int)
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * from tickets where id= id_ticket; 
    IF NOT FOUND THEN 
        RETURN FALSE;
    END IF;
    DELETE from tickets
    WHERE id = id_ticket;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<delete_ticket> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;

-- accepter un ticket et proposer une date --
CREATE OR REPLACE FUNCTION update_date_ticket(id_ticket int, dateItervention DATE, timeIntervention TIME)
    RETURNS BOOLEAN AS
$$
BEGIN
    PERFORM * from tickets where id= id_ticket; 
    IF NOT FOUND THEN 
        RETURN FALSE;
    END IF;
    UPDATE ticket SET date_intervention = dateIntervention, time_intervention = timeIntervention, status = 'accepted'
    WHERE id = id_ticket;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '<update_date_ticket> failed: %', SQLERRM;
        RETURN FALSE;
END
$$ LANGUAGE plpgsql;
