/**
 * Created by huynh on 16/12/18.
 */


/**
 * Si vous avez besoin de vous connecter à la base de données, n'oubliez pas d'import le 'pool' (cf. fichier 'routes')
 */
const passport = require('./../identification/passport'); //Module servant à vérifier que l'utilisateur est bien connecte
let pool = require('./../sql/database.js');
var fs = require('fs');
var path = require('path');

const multer = require('multer');

const upload = multer({
    dest: './uploads'
});

module.exports = function(app) {
    /**
     * Ecrire les routes ici
     */

    
    //getBuildings, 
    app.get("/ticket/create_ticket", passport.isAuthenticated, getBuildings, function(req, res)
    {
        res.render("./client/client-create-modify-ticket.ejs", {
            tickets:req.tickets,
            client: req.user
        });
    });

    app.post("/ticket/submit-change", passport.isAuthenticated, upload.single("file"), renameFile, createTicket, function(req, res){
        res.redirect('/client/profil');
    });

    app.post("/ticket/accepter/:id", passport.isAuthenticated, isAdmin, acceptTicket, function(req, res) {
        res.redirect('/admin/liste-tickets');
    });

    app.post("/ticket/refuser/:id", passport.isAuthenticated, isAdmin, declineTicket, function(req, res) {
        res.redirect('/admin/liste-tickets');
    });


/**
 * Ecrire les middlewares ici si nécessaire
 */

function isAdmin(req, res, next) {
    if (!req.user.admin) {
        res.redirect('/client/profil');
    }
    next();
}

function getBuildings(req, res, next) {
    pool.query('SELECT * FROM batiment WHERE adresse_email_client = $1', [req.user.email],
        function (err, result) {
            if (err) {
                console.error('[routes.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.tickets = result.rows.length > 0 ? result.rows : null;
                next();
            }
        })
}

function acceptTicket(req, res, next) {
    pool.query('UPDATE tickets SET status=$1, date_intervention=TO_DATE($2, \'DD/MM/YYYY\'), time_intervention=$3 WHERE id = $4', ['accepted', req.body.date, req.body.time, req.params.id],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}


function declineTicket(req, res, next) {
    pool.query('UPDATE tickets SET status=$1, reason_rejection=$2 WHERE id = $3', [ 'refused', req.body.reason, req.params.id],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function createTicket(req, res, next) {
    pool.query('INSERT INTO tickets VALUES(DEFAULT, $1, $2, $3, $4, $5, TO_DATE($6, \'DD/MM/YYYY\'), $7, $8, $9)',
                                                                                [req.filename, // to change
                                                                                req.body.building,
                                                                                req.body.orientation,
                                                                                req.body.owner_email,
                                                                                req.body.etage, // to change
                                                                                null, //to change
                                                                                null, //to change
                                                                                'reviewing', //to change
                                                                                req.body.description],
        function (err, result) {
            if (err) {
                console.error('[routes.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                console.log(result.err);
                next();
            }
        })
    }

    function renameFile(req, res, next) {
        req.filename = req.file.filename + ".png";
        if (path.extname(req.file.originalname).toLowerCase() === ".png") {
            fs.rename(req.file.path, "./public/img/" + req.filename, function (err) {
                if (err) {
                    console.log(err);
                    next();
                }
            })
        }
        next();
    }
}