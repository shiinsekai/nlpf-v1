/**
 * Created by huynh on 11/12/18.
 * Fichier relatif à tout ce qui est gestion du compte par un client
 */

/**
 * Si vous avez besoin de vous connecter à la base de données, n'oubliez pas d'import le 'pool' (cf. fichier 'routes')
 */
const passport = require('./../identification/passport'); //Module servant à vérifier que l'utilisateur est bien connecte
let pool = require('./../sql/database.js');
var dateFormat = require('dateformat');

module.exports = function(app) {
    /**
     * Ecrire les routes ici
     */
    require('./ticket')(app);

    app.get("/client/register", passport.isAuthenticated, function(req, res){
        res.render("./client/client-register.ejs", {
            client: req.user
        });
    });

    app.post("/client/register/submit", passport.isAuthenticated, registerClient, function(req, res) {
        res.redirect('/client/profil');
    });

    app.get("/client/profil", passport.isAuthenticated, getTickets, getBuildings, function(req, res){
        res.render("./client/client-profile.ejs", {
            client: req.user,
            tickets: req.tickets,
            batiments: req.batiments
        });
    });

    app.get("/client/ticket/:id", passport.isAuthenticated, getTicket, getTickets, function (req, res) {
        res.render("./client/ticket-detail.ejs", {
            client: req.user,
            ticket: req.ticket,
            tickets: req.tickets
        })
    });

    app.post("/client/modify/submit", passport.isAuthenticated, modifyUser, function (req, res) {
        res.redirect("/client/profil");
    });

    app.post("/client/batiment/add/submit", passport.isAuthenticated, addBuilding, function(req, res) {
        res.redirect('/client/profil');
    });

    app.get("/client/batiment/:id/delete", passport.isAuthenticated, deleteBuilding, function(req, res) {
        res.redirect('/client/profil');
    });

    app.post("/client/batiment/modify/submit", passport.isAuthenticated, modifyBuilding, function(req, res) {
        res.redirect('/client/profil');
    });

    app.post("/client/ticket/add/submit", passport.isAuthenticated, addTicket, function(req, res) {
        res.redirect('/client/profil');
    });

    app.post("/client/ticket/:id/modify", passport.isAuthenticated, modifyTicket, function(req, res) {
        res.redirect('/client/profil');    
    });
};

/**
 * Ecrire les middlewares ici si nécessaire
 */

function getTickets(req, res, next) {
    pool.query('select *, utilisateurs.nom as name from utilisateurs LEFT JOIN (select *, tickets.adresse_email_client as email, batiment.nom as bat, tickets.id as ticket from tickets left join batiment on id_batiment = batiment.id) as ticketbatiment ON utilisateurs.adresse_email = email where utilisateurs.adresse_email = $1;', [req.user.email],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.tickets = result.rows;
                next();
            }
        })
}

function modifyUser(req, res, next) {
    pool.query('UPDATE utilisateurs SET nom=$1, prenom=$2, telephone=$3 WHERE adresse_email=$4', [req.body.name, req.body.firstname, req.body.telephone, req.user.email],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        })
}


function getBuildings(req, res, next) {
    pool.query('SELECT * FROM batiment WHERE adresse_email_client = $1', [req.user.email],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.batiments = result.rows.length > 0 ? result.rows : null;
                next();
            }
        })
}

function getBuilding(req, res, next) {
    pool.query('SELECT * FROM batiment WHERE id = $1', [req.params.id],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.batiment = result.rows.length > 0 ? result.rows[0] : null;
                next();
            }
        })
}

function addBuilding(req, res, next) {
    pool.query('SELECT create_building($1, $2, $3, $4, $5)', [req.body.address,
                                                              req.body.postcode,
                                                              req.body.city,
                                                              req.user.email,
                                                              req.body.name],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.tickets = result.rows;
                next();
            }
        });
}

function addTicket(req, res, next) {
    pool.query('INSERT INTO tickets VALUES(DEFAULT, $1, $2, $3, $4, $5, TO_DATE($6, \'DD/MM/YYYY\'), $7, $8, $9)',
                                                                                ['image', // to change
                                                                                req.body.building,
                                                                                req.body.orientation,
                                                                                req.body.owner_email,
                                                                                req.body.etage, // to change
                                                                                req.body.date, //to change
                                                                                req.body.horaire, //to change
                                                                                'reviewing', //to change
                                                                                req.body.description],
        function (err, result) {
            if (err) {
                console.error('[routes.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                console.log(result.err);
                next();
            }
        })
}


function modifyBuilding(req, res, next) {
    pool.query('SELECT update_building($1, $2, $3, $4, $5)', [req.body.id,
                                                              req.body.address,
                                                              req.body.postcode,
                                                              req.body.city,
                                                              req.body.name],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function modifyTicket(req, res, next) {
    pool.query('UPDATE tickets SET orientation=$1,id_batiment=$2, description=$3, status=$4 WHERE id = $5', 
    [
    req.body.orientation,
    req.body.building,
    req.body.description,
    "reviewing",
    req.body.id ],
            function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function deleteBuilding(req, res, next) {
    pool.query('SELECT delete_building($1)', [req.params.id],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function registerClient(req, res, next) {
    pool.query('UPDATE utilisateurs SET nom=$1, prenom=$2, telephone=$3, admin=$4, blacklist=$5 WHERE adresse_email=$6',
                                                              [req.body.name,
                                                              req.body.firstname,
                                                              req.body.phone,
                                                              false,
                                                              false,
                                                              req.user.email],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function getTicket(req, res, next) {
    pool.query('select *, utilisateurs.nom as name from utilisateurs LEFT JOIN (select *, tickets.adresse_email_client as email, batiment.nom as bat, tickets.id as ticket from tickets left join batiment on id_batiment = batiment.id) as ticketbatiment ON utilisateurs.adresse_email = email where ticketbatiment.ticket = $1 and utilisateurs.adresse_email = $2;', [req.params.id, req.user.email],
        function (err, result) {
            if (err) {
                console.error('[client.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.ticket = result.rows[0];
                req.ticket.date_intervention = dateFormat(new Date(), "yyyy-mm-dd");
                next();
            }
        })
}