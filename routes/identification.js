/**
 * Created by huynh on 21/09/18.
 */

let pool = require('./../sql/database.js');
var passport = require('passport');
const url = require('url');

module.exports = function(app) {

    app.get('/logout', function(req, res) {
        req.logout();
        console.log("[identification.js] Disconnected");
        if (req.query.blacklist === 'true') {
            res.redirect('/access-denied');
        }
        else {
            res.redirect('/');
        }
    });


    app.get('/auth/outlook',
        passport.authenticate('windowslive', { scope: [
            'openid',
            'profile',
            'offline_access',
            'https://outlook.office.com/Mail.Read',
            'https://outlook.office.com/Calendars.ReadWrite'
            
        ] }),
        function(req, res){
            // The request will be redirected to Outlook for authentication, so
            // this function will not be called.
        });

    app.get('/auth/outlook/callback',
        passport.authenticate('windowslive', { failureRedirect: '/' }),
        checkEmail,
        function(req, res) {
            console.log('[identification.js] Connected');
            if (req.user_type === "blacklist") {
                res.redirect('/logout?blacklist=true');
            }
            else if (req.user_type === "new") {
                res.redirect('/client/register');
            }
            else if (req.user_type === "admin") {
                res.redirect('/admin/profil');
            }
            else {
                res.redirect('/client/profil');
            }
        });
};

function checkEmail(req, res, next) {
    pool.query('SELECT check_account($1)', [req.user.email], //checking whether it's a new user, a client, or an admin
        function (err, result) {
            if (err) {
                console.error('[routes.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.user_type = result.rows[0].check_account;
                next();
            }
        })
}
