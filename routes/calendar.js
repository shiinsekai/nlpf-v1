/**
 * Created by huynh on 11/12/18.
 * Fichier relatif à tout ce qui est gestion du calendrier par l'admin
 */

/**
 * Si vous avez de vous connecter à la base de données, n'oubliez pas d'import le 'pool' (cf. fichier 'routes')
 */
const passport = require('./../identification/passport'); //Module servant à vérifier que l'utilisateur est bien connecte
const graph = require('@microsoft/microsoft-graph-client');
let pool = require('./../sql/database.js');


module.exports = function(app) {
    /**
     *  Get the Calendar of the admin
     */
    app.get('/calendar', async function(req, res) {
        let parms = { title: 'Calendar', active: {calendar: true}};

        //const userName = req.cookies.graph_user_name;
        accessToken = req.user.accessToken;
        userName = req.user.outlook_id;
        if (accessToken && userName)
            parms.user = userName;
        
        //Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, req.user.accessToken);
            }
        });

        let start = new Date(new Date().setHours(0,0,0));
        let end = new Date(new Date(start).setDate(start.getDate() + 7));

        try {
            // Get the first 10 events for the week
            const result = await client
            /*.api('/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}')
            .top(10)
            .select('subject,start,end,attendees')
            .orderby('start/dateTime DESC')*/
            .api('/me/mailfolders/inbox/messages')
            .top(10)
            .get();
            console.log('[calendar.js] '.concat(result.value));
            parms.events = result.value;
            //res.render('calendar', parms);
        } catch(err) {
            console.log('[calendar.js] Get : '.concat(err.code).concat(' - ').concat(err.message));
        }
    })

    /**
     */
    app.get("/calendar/events", function (req, res) {
        if (!req.isAuthenticated()) {
            console.log('[calendar.js] Not authenticated');
            error: req.query.error
        }
        pool.query('SELECT id as title, id_batiment, orientation, adresse_email_client as client, etage, date_intervention as start FROM tickets WHERE status = $1', ['accepted'],
        function (err, result) {
            if (err) {
                console.error('[calendar.js]: Error while executing query ', err.stack);
            }
            else {
                req.tickets = result.rows.length > 0 ? result.rows : null;
                console.log("[calendar.js] : ", req.tickets);
                res.tickets =  req.tickets;
                return res;
            }
        })
        
    });
};

/**
{
  "Subject": "Discuss the Calendar REST API",
  "Body": {
    "ContentType": "HTML",
    "Content": "I think it will meet our requirements!"
  },
  "Start": {
      "DateTime": "2014-02-02T18:00:00",
      "TimeZone": "Pacific Standard Time"
  },
  "End": {
      "DateTime": "2014-02-02T19:00:00",
      "TimeZone": "Pacific Standard Time"
  }
}
 */

/**
 * Ecrire les les middlewares ici si nécessaire
 */