let pool = require('./../sql/database.js');
const url = require('url');
const querystring = require('querystring');
const passport = require('./../identification/passport');


module.exports = function(app) {

    require('./identification')(app);
    require('./admin')(app);
    require('./client')(app);

    app.get("/", function (req, res) {
        res.render("./home/index.ejs");
    });

    app.get("/access-denied", function (req, res) {
        res.render("./home/access-denied.ejs");
    })
};