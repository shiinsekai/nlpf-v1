/**
 * Created by huynh on 11/12/18.
 * Fichier relatif à tout ce qui est gestion par le profil admin
 */

/**
 * Si vous avez besoin de vous connecter à la base de données, n'oubliez pas d'import le 'pool' (cf. fichier 'routes')
 */
const passport = require('./../identification/passport'); //Module servant à vérifier que l'utilisateur est bien connecte
let pool = require('./../sql/database.js');
var dateFormat = require('dateformat');

module.exports = function(app) {
    require('./calendar')(app);
    /**
     * Ecrire les routes ici
     */

    app.get("/admin/profil", passport.isAuthenticated, isAdmin, getTickets, function(req, res){
        
        res.render("./administrator/administrator-profile.ejs", {
            admin: req.user,
            tickets: req.tickets
        });
    });

    app.get("/admin/liste-clients", passport.isAuthenticated, isAdmin, getClients, function(req, res){
        res.render("./administrator/clients-list.ejs", {
            admin: req.user,
            clients: req.clients
        });
    });

    app.get("/admin/info-client/:email", passport.isAuthenticated, isAdmin, getClient, getBatiments, function(req, res){
        res.render("./tmp/admin_client.ejs", {
            admin: req.user,
            client: req.client,
            batiments: req.batiments
        });
    });

    app.get("/admin/blacklist/:email", passport.isAuthenticated, isAdmin, blacklistUser, function(req, res){
        res.redirect('/admin/liste-clients');
    });

    app.get("/admin/unblacklist/:email", passport.isAuthenticated, isAdmin, unblacklistUser, function(req, res){
        res.redirect('/admin/liste-clients');
    });

    app.get("/admin/liste-tickets", passport.isAuthenticated, isAdmin, getTickets, function(req, res) {
        res.render("administrator/ticket-list.ejs", {
            admin: req.user,
            tickets: req.tickets
        });
    });

    app.get("/admin/info-ticket/:id", passport.isAuthenticated, isAdmin, getTicket, function(req, res) {
        res.render("admin/tickets_list.ejs", {
            admin: req.user,
            ticket: req.ticket
        });
    });

    app.get("/admin/ticket/:id", passport.isAuthenticated, isAdmin, getTicket, function(req, res) {
        res.render("./administrator/ticket-detail.ejs", {
            admin: req.user,
            ticket: req.ticket
        });
    });
};

/**
 * Ecrire les middlewares ici si nécessaire
 */

function isAdmin(req, res, next) {
    if (!req.user.admin) {
        res.redirect('/client/profil');
    }
    next();
}

function blacklistUser(req, res, next) {
    pool.query('UPDATE utilisateurs SET blacklist=$1 WHERE adresse_email=$2', [true, req.params.email],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function unblacklistUser(req, res, next) {
    pool.query('UPDATE utilisateurs SET blacklist=$1 WHERE adresse_email=$2', [false, req.params.email],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                next();
            }
        });
}

function getClients(req, res, next) {
    pool.query('SELECT * FROM utilisateurs WHERE admin = FALSE',
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.clients = result.rows;
                next();
            }
        })
}

function getClient(req, res, next) {
    pool.query('SELECT * FROM utilisateurs WHERE adresse_email = $1', [req.params.email],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.client = result.rows[0];
                next();
            }
        })
}

function getBatiments(req, res, next) {
    pool.query('SELECT * FROM batiment WHERE adresse_email_client = $1', [req.params.email],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.batiments = result.rows;
                next();
            }
        })
}

function getTickets(req, res, next) {
    pool.query('select *, utilisateurs.nom as name from utilisateurs LEFT JOIN (select *, tickets.adresse_email_client as email, batiment.nom as bat, tickets.id as ticket from tickets left join batiment on id_batiment = batiment.id) as ticketbatiment ON utilisateurs.adresse_email = email;',
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.tickets = result.rows;
                for (var i = 0; i < req.tickets.length; i++) {
                    if (req.tickets[i].date_intervention != null)
                        req.tickets[i].date_intervention = dateFormat(req.tickets[i].date_intervention, "yyyy-mm-dd");
                }
                next();
            }
        })
}

function getTicket(req, res, next) {
    pool.query('select *, utilisateurs.nom as name from utilisateurs LEFT JOIN (select *, tickets.adresse_email_client as email, batiment.nom as bat, tickets.id as ticket from tickets left join batiment on id_batiment = batiment.id) as ticketbatiment ON utilisateurs.adresse_email = email where ticketbatiment.ticket = $1', [req.params.id],
        function (err, result) {
            if (err) {
                console.error('[admin.js]: Error while executing query ', err.stack);
                next();
            }
            else {
                req.ticket = result.rows[0];
                req.ticket.date_intervention = dateFormat(new Date(), "yyyy-mm-dd");
                next();
            }
        })
}


