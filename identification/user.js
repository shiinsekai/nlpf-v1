/**
 * Created by huynh on 21/09/18.
 */

let pool = require('./../sql/database.js');

function User()
{
    this.outlook_id = 0;
    this.name = '';
    this.firstname = '';
    this.phone = '';
    this.email = '';
    this.admin = false;
    this.blacklist = false;
    this.accessToken = '';
    this.save = function (next) {
        var id = this.outlook_id;
        var mail = this.email;
        var accessToken = this.accessToken;
        pool.query('SELECT update_user_id($1, $2, $3)', [mail, id, accessToken],
            function (err, result) {
                if (err) {
                    return console.error('[user.js]: Error while executing query ', err.stack);
                }
                else {
                    console.log('[user.js] Updated outlook ID for user <' + mail + '> successfully');
                    return next(err);
                }
            });
    }
}

User.findById = function(id, callback)
{
    pool.query('SELECT * FROM utilisateurs WHERE outlook_id=$1', [id], function(err, result) {
       if (err) {
           return callback(err, null);
       }
       if (result.rows.length > 0) {
           var user = new User();
           user.email = result.rows[0].adresse_email;
           user.name = result.rows[0].nom;
           user.phone = result.rows[0].telephone;
           user.firstname = result.rows[0].prenom;
           user.outlook_id = result.rows[0].outlook_id;
           user.admin = result.rows[0].admin;
           user.blacklist = result.rows[0].blacklist;
           user.accessToken = result.rows[0].access_token;
                        
           return callback(null, user);
       }
    });
};

module.exports = User;