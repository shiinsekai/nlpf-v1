/**
 * Created by huynh on 21/09/18.
 */

var passport = require('passport');
var OutlookStrategy = require('passport-outlook').Strategy;
var User = require('./user');
let pool = require('./../sql/database.js');

passport.serializeUser(function(user, done) {
    done(null, user.outlook_id);
});

passport.deserializeUser(function(obj, done) {
    User.findById(obj, function(err, user) {
        done(err, user);
    });
});

passport.use(new OutlookStrategy({
        clientID: global.gConfig.outlook_api_id,
        clientSecret: global.gConfig.outlook_api_secret,
        callbackURL: global.gConfig.outlook_callback_url
    },
    function(accessToken, refreshToken, profile, done) {
        // asynchronous verification, for effect...
        process.nextTick(function () {
            pool.query('SELECT check_account($1)', [profile._json.EmailAddress],
                function (err, result) {
                    if (err) {
                        if (err) {
                            console.log('[passport.js] Error occured: ' + err);
                        }
                        return done(err, null);
                    }
                    else {
                        var adm = new User();
                        adm.accessToken = accessToken;
                        adm.outlook_id = profile._json.Id;
                        adm.name = profile.displayName.split(" ")[1];
                        adm.firstname = profile.displayName.split(" ")[0];
                        adm.email = profile._json.EmailAddress;

                        adm.save(function(err) {
                            if (err) {
                                console.log("[user.js] Error occurred during update of outlook ID for user <" + profile.email + ">");
                                throw err;
                            }
                            return done(null, adm);
                        });
                    }
                });
        });
    }
));

module.exports = {
    isAuthenticated: function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    }
};
