Array.prototype.contains = function(element) {
  return this.indexOf(element) > -1;
}

const available_env = ['local', 'development', 'production'];

var env = 'local';

if (available_env.contains(process.argv[3])) {
  env = process.argv[3];
}
else {
  console.log('[server.js] Unknown environment <' + process.argv[3] + '>');
  console.log('[server.js] Using default environment <local>');
  console.log('Here\'s the available environment: <local>, <development>, <production>');
}

module.exports = { env: env };

process.env.NODE_ENV = env;

const config = require('./config/config.js');

var fs = require('fs');
var path = require('path');

var express = require('express');

var bodyParser = require('body-parser');

var passport = require('passport');

var app = express();

var session = require('express-session');

var https = require('https');

var privateKey = fs.readFileSync(global.gConfig.private_key, 'utf8');
var certificate = fs.readFileSync(global.gConfig.certificate, 'utf8');

var credentials = {key: privateKey, cert: certificate};

require('./identification/passport');

app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.use(session({
  secret: 'Best PAE 3v3r',
  resave: false,
  cookie: { maxAge: 30 * 24 * 60 * 60 * 60 * 1000 }
  }));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var httpsServer = https.createServer(credentials, app);

const port = process.env.PORT || global.gConfig.node_port;

httpsServer.listen(port);

require('./routes/routes.js')(app);
